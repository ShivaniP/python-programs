def pentagonal(num):
  return list( n * (3 *n - 1)//2  for n in range(num))

def penta_Pairs(num):
  return list((i,j) for i in range(num) for j in range(num) if pentagonal(i) + pentagonal(j) in pentagonal(num) and abs(pentagonal(i) - pentagonal(j)) in pentagonal(num))

def min_abs(num):
  return min(list(abs(pentagonal(i) - pentagonal(j))  for i in range(num) for j in range(num) if abs(pentagonal(i) - pentagonal(j)) in pentagonal(num)))

