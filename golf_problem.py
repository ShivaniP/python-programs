def golf_problem():
  expected_hits = list(map(int, input().split()))
  golf_player = list(map(str, input().split()))
  golf_terms = {'eagle':-2,'birdie':-1,'bogey':1,'double-bogey':2,'triple-bogey':3,'albatross':-3,'par':0}
  golf_points = list(golf_terms.get(i) for i in golf_player)
  over_Par = len(list(k for k in golf_player if  golf_terms.get(k) > 0 ))
  under_Par = len(list(k for k in golf_player if golf_terms.get(k) < 0))
  on_Par = len(list(k for k in golf_player if golf_terms.get(k) == 0))
  score = {'over_Par':over_Par, 'under_Par':under_Par}
  total_score = max(zip(score.values(),score.keys()))
  print(str(sum(golf_points)) + " " + total_score[1])

(golf_problem())
