import random
values = {'1' : 1, '2' : 2, '3' : 3, '4' : 4, '5' : 5, '6' : 6, '7' : 7, '8' : 8, '9' : 9, 'T' : 10, 'J' : 11, 'Q' : 12, 'K' : 13, 'A' : 14}
player1_list = ['2','3','4','5','6','7','8','9','T','J','Q','K','A']

def ideal_cards():
    cards = ['2','3','4','5','6','7','8','9','T','J','Q','K','A']
    Diamonds = random.choice(cards)
    cards.remove(Diamonds)
    return values.get(Diamonds)


def player_two():
    player2_var = game()
    #player2_var = random.choice(player2_list)
    return values.get(player2_var)

def game():
    player2_list = ['2','3','4','5','6','7','8','9','T','J','Q','K','A']
    print("Select Strategy:\n1.Random\n2.Higher than your card\n3.Lower than your card")
    game = int(input())
    if game == 1:
        player2_var1 = random.choice(player2_list)
        player2_list.remove(player2_var1)
        return player2_var1
        
    elif game == 2:
        player2_var1 = diamond_higher_than(ideal_cards(), player2_list)
        player2_list.remove(player2_var1)
        return player2_var1
        
    elif game == 3:
        player2_var1 = diamond_lower_than(ideal_cards(), player2_list)
        player2_list.remove(player2_var1)
        return player2_var1
        
    else:
        print("enter valid choice")


def player_one():
     print(player1_list)
     player1_var = input()
     print("your input is " +player1_var)
     player1_list.remove(player1_var)
     return values.get(player1_var)

def diamond_higher_than(ideal_choice , player_2_list) :

        if ideal_choice in player_2_list :
                choice_list = player_2_list[player_2_list.index(ideal_choice):]

                if len(choice_list) == 1 :
                        player_2_choice = player_2_list[len(player_2_list) - 1]
                else :
                        player_2_choice = random.choice(choice_list)

        else :
                choice_list = player_2_list
                player_2_choice = random.choice(choice_list)

        return player_2_choice


def compare(choice1, choice2,Diamonds):
    Player1_Sum = 0
    Player2_Sum = 0

    if choice1 > choice2:
       print("Player1 you won Diamonds")
       Player1_Sum = Player1_Sum + Diamonds
    elif choice1 < choice2:
       print("Player2 you won Diamonds")
       Player2_Sum = Player2_Sum + Diamonds
    else:
       print("Player1 you won Diamonds")
       print("Player2 you won Diamonds")
       Player1_Sum =  Player1_Sum + Diamonds//2
       Player2_Sum = Player2_Sum + Diamonds//2 
    return Player1_Sum, Player2_Sum        

for i in range(13):
  print("Player 1 : your turn")
  print(compare(player_one(), player_two(),ideal_cards()))  
 
