def factorial(n:int) -> int:
    if n in [0,1]:
        return 1
    if n > 1:
        r = 1
        fact = 1
        while(r <= n):
            fact = fact * r
            r += 1
        return[int(x) for x in str(fact)]
def sum_of_fact(n:int) -> int:
    return sum(factorial(n))
            
