from itertools import permutations
def Min_Max():
  num_input = list(map(int, input().split()))
  permutations_num = list(permutations(num_input,len(num_input)-1))
  print(str(min(list(sum(i) for i in permutations_num)))+" "+str(max(list(sum(i) for i in permutations_num))))
Min_Max()