money = list(map(int,input().split()))

def transaction():
    if(money[0] <= money[1] and money[0] % 5 == 0):
        return (money[1] - 0.5 - money[0])
    else:
        return money[1]

print(transaction())
