def sum_powers():
  return sum(set(pow(a,b) for a in range(1,1000) for b in range(1,1000)))

print(sum_powers())
