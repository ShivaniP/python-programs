from itertools import permutations,combinations_with_replacement

def possible_words(characters,Dict):
    res_words = []
    per_words = []
    for i in range(len(characters)):
        res = list(combinations_with_replacement(characters,i + 1))
        words = [''.join(i) for i in res]
        for p in permutations(words):
            per_words.append(p)
        for i in per_words :
            if i in Dict:
                res_words.append(i)
    return res_words


