from itertools import combinations_with_replacement

def is_Prime(n:int) -> int:
    if n in [2,3,5,7]:
        return True
    if n % 2 == 0:
        return False
    r = 3
    while r * r <= n:
        if n % r == 0:
            return False
        r = r + 2
    return True


def comb_of_prime(num):
    l_prime = list(i for i in range(2,num) if is_Prime(i))
    return len(list(combinations_with_replacement(l_prime,3)))
            
