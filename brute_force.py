def brute_force():
    number_list = []
    for i in range(int(input())):
        num = int(input())
        number_list.append(num)
    for i in number_list:
        if i != 42:
            return i
        else:
            break

print(brute_force())
