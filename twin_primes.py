def Is_TwinPrime(num1,num2):
    if(num1 < num2):
        if(Is_Prime(num2)- Is_Prime(num1) == 0 and num2 -num1 == 2):
            return True
        else:
            return False
    else:
        if(Is_Prime(num1) - Is_Prime(num2) == 0 and num1 - num2 == 2):
            return True
        else:
            return False
def Prime_numbers(num):
    list1 = list(i for i in range(1,num+1) if num % i == 0)
    return len(list1)
def Is_Prime(num):
    if(Prime_numbers(num) == 2):
        return True
    else:
        return False


