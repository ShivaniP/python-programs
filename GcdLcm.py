import math
def gcdlcm():
    n = int(input())
    for i in range(n):
        nums = list(map(int,input().split()))
        gcd = math.gcd(nums[0],nums[1])
        lcm = (nums[0] * nums[1])//gcd
        print(str(gcd)+" "+str(lcm))
gcdlcm()