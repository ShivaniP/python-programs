from itertools import islice,chain
alphabets = list("abcdefghijklmnopqrstuvwxyz")

def shift_alp(name,shift):
  list_name = list(name)
  res = [list(islice(alphabets,j+shift,j+shift+1))  for i in range(len(list_name)) for j in range(len(alphabets)) if alphabets[j] == name[i]]
  return ''.join(list(chain.from_iterable(res)))

print(shift_alp("abc",3))
  
