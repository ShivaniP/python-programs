def low_upp(string):
    return (''.join([i.lower() if i.isupper() else i.upper() for i in string]))

print(low_upp("aSd"))

