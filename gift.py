from itertools import combinations
def gift():
    n = list(input())
    count = 0
    gift = list(combinations(n,2))
    for i in range(len(gift)):
        if(gift[i][0] != gift[i][1]):
          count = count + 1
    return (count + 1)
print(gift())