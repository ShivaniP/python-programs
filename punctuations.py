def Punctuation(string):
    punctations = ",/'_-!@#$%^&*(){}[]:;'"
    no_punctuation = " "
    for char in string:
        if char not in punctations:
            no_punctuation = no_punctuation + char
    return no_punctuation

