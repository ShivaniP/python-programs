def pronic_number(num):
    if(num > 0):
        return generating_pronic_sequence(num)
    else:
        return False
def generating_pronic_sequence(num):
    return [i*(i + 1) for i in range(num)]
print(pronic_number(4))

