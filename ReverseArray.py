def ReverseArray():
    n = int(input())
    k = list(map(int,input().split()))
    rev = list(reversed(k))
    return (" ".join(str(i) for i in rev))
print(ReverseArray())